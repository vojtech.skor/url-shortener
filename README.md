# The world's simplest URL Shortener

This exercise will implement a very simple HTTP URL shortener service.

## Part 1: Web Service API

SPRING-BOOT and Kotlin is used to create HTTP service that allows creating and visiting short URLs.
The service utilization REST via HTTP.

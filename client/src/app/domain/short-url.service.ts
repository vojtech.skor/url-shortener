import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {map, Observable} from "rxjs";
import {Urls} from "./Urls";
import {FullUrl} from "./FullUrl";


@Injectable({providedIn: 'root'})
export class ShortUrlService{

  private readonly SERVICES_ENDPOINT ="http://localhost:8080/short"

  constructor(private http: HttpClient) {
  }

  shortUrl(url: FullUrl) : Observable<Urls> {
    return this.http.post(`${this.SERVICES_ENDPOINT}`, url).pipe(map(data => Urls.fromDTO(data)))
  }
}

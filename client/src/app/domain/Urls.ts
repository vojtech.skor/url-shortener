
export class Urls {
  constructor(public full: string, public short: string) {
  }

  static fromDTO(dto: any): Urls {
    const typedDTO = dto as Urls
    return new Urls(typedDTO.full, typedDTO.short)
  }
}

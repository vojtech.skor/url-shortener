import {Component, Inject} from '@angular/core';
import {ShortUrlService} from "./domain/short-url.service";
import {FullUrl} from "./domain/FullUrl";
import { DOCUMENT } from '@angular/common';
import {Clipboard} from '@angular/cdk/clipboard';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  value : string  = ''
  isShorten: boolean = false

  constructor(private service: ShortUrlService,
              @Inject(DOCUMENT) private document: Document,
              private clipboard: Clipboard) {
  }

  // TODO refactor, this force update of html tree in each cycle
  isValueNotEmpty() : boolean {
    return this.value !== ''
  }

  isShortenButtonVisible(): boolean {
    return this.value === '' || !this.isShorten
  }

  isCopyButtonVisible() : boolean {
    return !this.isShortenButtonVisible()
  }

  onClickClear() {
    this.value=''
    this.isShorten = false
  }

  onClickShorten() {
     this.service.shortUrl(new FullUrl(this.value)).subscribe(
       shortenUrls => {
         this.value = this.document.location.href + shortenUrls.short
         this.isShorten = true
       }
     )
  }

  onClickCopy() {
    this.clipboard.copy(this.value);
  }
}

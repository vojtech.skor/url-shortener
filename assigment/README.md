
# The world's simplest URL Shortener

This exercise implements a very simple HTTP URL shortener service.

## Part 1: Web Service API

Write an HTTP service that allows creating and visiting short URLs. You can choose the language you are most comfortable with.

### Requirements

 * When a URL is submitted, a much shorter URL is returned, the shorter the better
 * The same input URL should always result in the same short URL
 * The input URL must be validated. An invalid URL should result in an HTTP 400 error
 * Assume there will be a need to support billions of URLs
 * When a short URL is visited, the user is redirected to the associated input URL. If it does not exist, an HTTP 404 error should be returned

## Part 2: User Interface

Write a web user interface for shortening URLs.

## URL Shortener UI

### Requirements

 * The UI consists of a vertically and horizontally centered panel containing a single field where a user can enter a URL to shorten
 * When the user enters a URL then clicks the "Shorten" button, the URL is displayed in the field
 * An "X" is available to allow the user to restore the field and enter a new URL
 * A "Copy" button is available to allow the user to copy the URL to the clipboard
 * Try and make it look as close as possible to the images in the `screenshots` directory

package com.skorvaga.urlshortener

import com.skorvaga.urlshortener.interfaces.UrlsDTO
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatusCode
import org.springframework.http.MediaType

@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY, connection = EmbeddedDatabaseConnection.H2)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UrlShortenerApplicationTests(
		@LocalServerPort
		private val port: Int,
		@Autowired
		private val restTemplate: TestRestTemplate
) {
	private val headers = HttpHeaders()

	init {
		headers.contentType = MediaType.APPLICATION_JSON
	}

	@Test
	fun contextLoads() {
	}

	@Test
	fun `given reachable link when shorting url then response is returned`(){
		val testUrl = testData("http://google.com")
		val request = HttpEntity<String>(testUrl, headers)
		val expected = UrlsDTO("http://google.com", "c7b920f")

		assertThat(restTemplate.postForEntity("http://localhost:$port/short", request,UrlsDTO::class.java).body,
				`is`(expected))
	}

	@Test
	fun `given invalid url when shorting url then 400 is returned`(){
		val testUrl = testData("htp://google.com")
		val request = HttpEntity<String>(testUrl, headers)

		assertThat(restTemplate.postForEntity("http://localhost:$port/short", request,String::class.java).statusCode,
				`is`(HttpStatusCode.valueOf(400)))
	}

	@Test
	fun `given stored short url when retrieving url by short name then original url is returned`(){
		val testUrl = testData("http://google.com")
		val requestToStore = HttpEntity<String>(testUrl, headers)
		val body = checkNotNull(
				restTemplate.postForEntity("http://localhost:$port/short", requestToStore,UrlsDTO::class.java).body)

		assertThat(restTemplate.getForEntity("http://localhost:$port/short/${body.short}", String::class.java).body,
				`is`("http://google.com"))
	}

	@Test
	fun `given existing short when trying shorten origin url again then the same short is returned`(){
		val testUrl = testData("http://google.com")
		val requestToStore = HttpEntity<String>(testUrl, headers)
		restTemplate.postForEntity("http://localhost:$port/short", requestToStore,UrlsDTO::class.java)

		val expected = UrlsDTO("http://google.com", "c7b920f")

		assertThat(restTemplate.postForEntity("http://localhost:$port/short", requestToStore,UrlsDTO::class.java).body,
				`is`(expected))
	}

	@Test
	fun `when try retrieving non existing short name then error 404 is returned`(){
		assertThat(restTemplate.getForEntity("http://localhost:$port/short/BBBB", String::class.java).statusCode,
		`is`(HttpStatusCode.valueOf(404)))
	}

	private fun testData(url: String) = "{ \"full\": \"$url\" }"
}

CREATE TABLE shorten_url(
   full_url TEXT NOT NULL UNIQUE,
   short_url TEXT,
   PRIMARY KEY (short_url)
);

package com.skorvaga.urlshortener.domain

import com.skorvaga.urlshortener.infrastructure.UrlsRepository
import org.springframework.stereotype.Component
import java.net.URLEncoder
import java.nio.charset.StandardCharsets
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

// source
// https://medium.com/@sandeep4.verma/system-design-scalable-url-shortener-service-like-tinyurl-106f30f23a82

private const val SHORT_URL_CHAR_SIZE = 7
@Component
class MD5Utils(val repository: UrlsRepository): ShortUrlGenerator {

    fun convert(longURL: String): String {
        return try {
            // Create MD5 Hash
            val digest = MessageDigest.getInstance("MD5")
            digest.update(longURL.toByteArray())
            val messageDigest = digest.digest()
            // Create Hex String
            val hexString = StringBuilder()
            for (b in messageDigest) {
                hexString.append(Integer.toHexString(0xFF and b.toInt()))
            }
            hexString.toString()
        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException(e)
        }
    }

    override fun generateShortUrl(longURL: FullUrl): ShortUrl {
        val hash = convert(longURL.value)
        val numberOfCharsInHash = hash.length
        var counter = 0
        while (counter < numberOfCharsInHash - SHORT_URL_CHAR_SIZE) {
            if (!exists(hash.substring(counter, counter + SHORT_URL_CHAR_SIZE))) {
                return ShortUrl(hash.substring(counter, counter + SHORT_URL_CHAR_SIZE))
            }
            counter++
        }
        val encodedUrl = URLEncoder.encode(longURL.value, StandardCharsets.UTF_8)
        return ShortUrl(encodedUrl)
    }

    private fun exists(shortUrl: String): Boolean {
        return this.repository.findByShortUrl(ShortUrl(shortUrl)) != null
    }
}

package com.skorvaga.urlshortener.domain

import java.net.MalformedURLException
import java.net.URISyntaxException
import java.net.URL

data class ShortUrl(val value: String)
data class FullUrl(val value: String) {
    init {
        if(!isValidURL(value)) throw InvalidUrlException("Url $value is not valid.")
    }
}

data class Urls(val fullUrl: FullUrl, val short: ShortUrl)

// source: https://www.baeldung.com/java-validate-url
private fun isValidURL(url: String) : Boolean {
    return try {
        URL(url).toURI()
        true
    } catch (e: MalformedURLException) {
        false
    } catch (e: URISyntaxException) {
        false
    }
}

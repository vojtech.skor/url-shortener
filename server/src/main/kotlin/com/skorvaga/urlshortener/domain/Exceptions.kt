package com.skorvaga.urlshortener.domain


class InvalidUrlException(message: String): Exception(message)

class ShortUrlDoesNotExistException(message: String): Exception(message)

package com.skorvaga.urlshortener.domain

interface ShortUrlGenerator {
    fun generateShortUrl(longURL: FullUrl): ShortUrl
}
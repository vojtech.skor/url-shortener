package com.skorvaga.urlshortener.domain

import com.skorvaga.urlshortener.infrastructure.UrlsRepository
import jakarta.transaction.Transactional
import org.springframework.stereotype.Service

@Service
class UrlShortenerService(val repository: UrlsRepository,
                          val generator: ShortUrlGenerator) {

    @Transactional
    fun handleShortingProcess(origin: FullUrl): Urls {
        repository.findByFullUrl(origin)?.let { return it }

        val urls = Urls(origin, createShort(origin))
        repository.save(urls)
        return urls
    }

    fun getFullUrl(short : ShortUrl) : FullUrl {
        repository.findByShortUrl(short)?.let{ return it.fullUrl }

        throw ShortUrlDoesNotExistException("Short url ${short.value} does not exist." )
    }

    private fun createShort(origin: FullUrl) : ShortUrl = this.generator.generateShortUrl(origin)
}

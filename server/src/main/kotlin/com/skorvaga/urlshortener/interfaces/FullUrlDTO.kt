package com.skorvaga.urlshortener.interfaces

import com.fasterxml.jackson.annotation.JsonProperty
import com.skorvaga.urlshortener.domain.FullUrl

data class FullUrlDTO(
        @JsonProperty("full")
        val full:String,
) {
    fun toDomain() = FullUrl(this.full)
}

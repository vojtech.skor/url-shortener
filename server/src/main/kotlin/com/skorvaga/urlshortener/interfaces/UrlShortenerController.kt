package com.skorvaga.urlshortener.interfaces

import com.skorvaga.urlshortener.domain.ShortUrl
import com.skorvaga.urlshortener.domain.UrlShortenerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.util.MimeTypeUtils
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/short")
@CrossOrigin
class UrlShortenerController(@Autowired val service: UrlShortenerService) {

    @PostMapping(consumes = [MimeTypeUtils.APPLICATION_JSON_VALUE], produces = [MimeTypeUtils.APPLICATION_JSON_VALUE])
    fun makeShort(@RequestBody origin: FullUrlDTO): UrlsDTO {
        val longUrl = origin.toDomain()
        val urls  = service.handleShortingProcess(longUrl)

        return UrlsDTO.fromDomain(urls)
    }

    @GetMapping("{shortUrl}")
    fun getFullUrl(@PathVariable("shortUrl") shortUrl: String) : String =
            service.getFullUrl(ShortUrl(shortUrl)).value
}

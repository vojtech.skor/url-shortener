package com.skorvaga.urlshortener.interfaces

import com.skorvaga.urlshortener.domain.InvalidUrlException
import com.skorvaga.urlshortener.domain.ShortUrlDoesNotExistException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler


@ControllerAdvice
class ShortUrlExceptionHandlers : ResponseEntityExceptionHandler() {

    @ExceptionHandler
    fun handle(ex: InvalidUrlException) = ResponseEntity(ex.message, HttpStatus.BAD_REQUEST)

    @ExceptionHandler
    fun handle(ex: ShortUrlDoesNotExistException) = ResponseEntity(ex.message, HttpStatus.NOT_FOUND)
}


package com.skorvaga.urlshortener.interfaces

import com.fasterxml.jackson.annotation.JsonProperty
import com.skorvaga.urlshortener.domain.FullUrl
import com.skorvaga.urlshortener.domain.ShortUrl
import com.skorvaga.urlshortener.domain.Urls

data class UrlsDTO(
        @JsonProperty("full")
        val full: String,
        @JsonProperty("short")
        val short: String) {

    constructor(fullUrl: FullUrl, short: ShortUrl): this(fullUrl.value, short.value)
    constructor(urls: Urls): this(urls.fullUrl, urls.short)
    companion object {
        fun fromDomain(domain: Urls) = UrlsDTO(domain)
    }
}

package com.skorvaga.urlshortener.infrastructure

import com.skorvaga.urlshortener.domain.FullUrl
import com.skorvaga.urlshortener.domain.ShortUrl
import com.skorvaga.urlshortener.domain.Urls
import jakarta.persistence.*
import java.util.*

@Entity
@Table(name = "shorten_url")
class UrlsEntity(
        @Column(name = "full_url", unique=true)
        val fullUrl: String,
        @Id
        @Column(name = "short_url", unique=true)
        val shortUrl: String,
) {
    constructor(fullUrl: FullUrl, shortUrl: ShortUrl): this(fullUrl.value, shortUrl.value)
    constructor(urls: Urls) : this(urls.fullUrl, urls.short)


    fun toDomain() = Urls(FullUrl(this.fullUrl), ShortUrl(this.shortUrl))

    companion object {
        fun fromDomain(domain: Urls) = UrlsEntity(domain)
    }
}

package com.skorvaga.urlshortener.infrastructure

import com.skorvaga.urlshortener.domain.FullUrl
import com.skorvaga.urlshortener.domain.ShortUrl
import com.skorvaga.urlshortener.domain.Urls

interface UrlsRepository {
    fun findByFullUrl(origin: FullUrl): Urls?

    fun findByShortUrl(short: ShortUrl): Urls?

    fun save(urls: Urls)
}

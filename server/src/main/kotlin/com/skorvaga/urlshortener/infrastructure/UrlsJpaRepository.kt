package com.skorvaga.urlshortener.infrastructure

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UrlsJpaRepository : JpaRepository<UrlsEntity, String> {
    fun findByFullUrl(fullUrl: String) : UrlsEntity?

    fun findByShortUrl(fullUrl: String) : UrlsEntity?
}
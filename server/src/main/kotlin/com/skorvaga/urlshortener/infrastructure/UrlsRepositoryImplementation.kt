package com.skorvaga.urlshortener.infrastructure

import com.skorvaga.urlshortener.domain.FullUrl
import com.skorvaga.urlshortener.domain.ShortUrl
import com.skorvaga.urlshortener.domain.Urls
import org.springframework.stereotype.Component

@Component
class UrlsRepositoryImplementation(val repository: UrlsJpaRepository): UrlsRepository {
    override fun findByFullUrl(origin: FullUrl): Urls? =
        repository.findByFullUrl(origin.value)?.toDomain()


    override fun findByShortUrl(short: ShortUrl): Urls? = repository.findByShortUrl(short.value)?.toDomain()

    override fun save(urls: Urls) { repository.save(UrlsEntity.fromDomain(urls)) }
}